﻿using System;
namespace LayoutTest2
{
	public class Expander
	{
		static int count;
		public int Count { get; }
		public Expander()
		{
			Count = ++count;
		}

		public bool IsExpanded { get; set; }
	}
}

