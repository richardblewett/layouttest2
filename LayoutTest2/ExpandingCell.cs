﻿using System;

using Foundation;
using UIKit;

namespace LayoutTest2
{
	public partial class ExpandingCell : UICollectionViewCell
	{
		public static readonly NSString Key = new NSString("ExpandingCell");
		public static readonly UINib Nib;

		static ExpandingCell()
		{
			Nib = UINib.FromName("ExpandingCell", NSBundle.MainBundle);
		}

		protected ExpandingCell(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}

		internal void Apply(Expander expander)
		{
			label.Text = expander.Count.ToString();
		}
	}
}
