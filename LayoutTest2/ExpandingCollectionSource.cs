﻿using System;
using System.Collections.Generic;
using Foundation;
using UIKit;

namespace LayoutTest2
{
	public class ExpandingCollectionSource : UICollectionViewSource
	{
		List<Expander> contents;
		readonly UICollectionView collectionView;

		public ExpandingCollectionSource(UICollectionView collectionView)
		{
			this.collectionView = collectionView;
			collectionView.RegisterNibForCell(ExpandingCell.Nib, ExpandingCell.Key);

			contents = new List<Expander>();
		}

		public override UICollectionViewCell GetCell(UICollectionView collectionView, Foundation.NSIndexPath indexPath)
		{
			var cell = collectionView.DequeueReusableCell(ExpandingCell.Key, indexPath) as ExpandingCell;

			cell.Apply(contents[indexPath.Row]);

			return cell;
		}

		public IEnumerable<Expander> GetItems()
		{
			return contents;
		}

		public override nint GetItemsCount(UICollectionView collectionView, nint section)
		{
			return contents.Count;
		}

		public override nint NumberOfSections(UICollectionView collectionView)
		{
			return 1;
		}

		public void AddData(IEnumerable<Expander> data)
		{
			contents = new List<Expander>(data);

			collectionView.ReloadData();
		}

		public override void ItemSelected(UICollectionView collectionView, NSIndexPath indexPath)
		{
			contents[indexPath.Row].IsExpanded = !contents[indexPath.Row].IsExpanded;

			this.collectionView.PerformBatchUpdates(() => {}, _ => { });
		}

		public void ExpandAll()
		{
			foreach (var item in contents)
			{
				item.IsExpanded = true;
			}
			this.collectionView.PerformBatchUpdates(() => { }, _ => { });
		}

		public void CollapseAll()
		{
			foreach (var item in contents)
			{
				item.IsExpanded = false;
			}
			this.collectionView.PerformBatchUpdates(() => { }, _ => { });
		}
	}
}

