﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoreGraphics;
using Foundation;
using UIKit;

namespace LayoutTest2
{
	public class ExpandingLayout : UICollectionViewLayout
	{
		readonly nfloat collapsedItemHeight;
		readonly nfloat separator;
		nfloat contentHeight;
		List<UICollectionViewLayoutAttributes> attributes = new List<UICollectionViewLayoutAttributes>();

		public ExpandingLayout(nfloat collapsedItemHeight, nfloat separator)
		{
			this.separator = separator;
			this.collapsedItemHeight = collapsedItemHeight;
		}

		public override void PrepareLayout()
		{
			base.PrepareLayout();

			nfloat yOffset = 0;
			nfloat width = this.CollectionView.Bounds.Width;

			var source = this.CollectionView.Source as ExpandingCollectionSource;
			var sourceItems = source.GetItems().ToList();

			attributes.Clear();

			for (int i = 0; i < this.CollectionView.NumberOfItemsInSection(0); i++)
			{
				NSIndexPath path = NSIndexPath.FromItemSection(i, 0);

				var attr = UICollectionViewLayoutAttributes.CreateForCell(path);

				var vm = sourceItems[path.Row];

				var frame = new CGRect(0, yOffset, width, vm.IsExpanded ? collapsedItemHeight * 2 : collapsedItemHeight);

				attr.Frame = frame;

				attributes.Add(attr);

				yOffset += frame.Height + separator;
			}

			contentHeight = yOffset;
		}

		public override bool ShouldInvalidateLayoutForBoundsChange(CGRect newBounds)
		{
			return true;
			return base.ShouldInvalidateLayoutForBoundsChange(newBounds);
		}
		public override CGSize CollectionViewContentSize
		{
			get
			{
				return new CGSize(this.CollectionView.Bounds.Width, contentHeight);
			}
		}

		public override UICollectionViewLayoutAttributes LayoutAttributesForItem(NSIndexPath indexPath)
		{
			return attributes[indexPath.Row];
		}

		public override UICollectionViewLayoutAttributes[] LayoutAttributesForElementsInRect(CGRect rect)
		{
			var attrsInRect = attributes.Where(a => a.Frame.IntersectsWith(rect));

			return attrsInRect.ToArray();
		}
	}
}

