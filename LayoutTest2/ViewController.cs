﻿using System;
using System.Collections.Generic;
using CoreGraphics;
using UIKit;

namespace LayoutTest2
{
	public partial class ViewController : UIViewController
	{
		List<Expander> stuff = new List<Expander>();
		protected ViewController(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.

			for (int i = 0; i < 100; i++)
			{
				stuff.Add(new Expander());
			}

			var source = new ExpandingCollectionSource(items);

			var layout = new ExpandingLayout(80, 5);

			expand.TouchUpInside += (sender, e) => source.ExpandAll();
			collapse.TouchUpInside += (sender, e) => source.CollapseAll();


			items.CollectionViewLayout = layout;

			items.Source = source;

			source.AddData(stuff);

			layout.InvalidateLayout();
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}
	}
}

