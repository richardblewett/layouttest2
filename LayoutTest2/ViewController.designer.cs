// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace LayoutTest2
{
	[Register ("ViewController")]
	partial class ViewController
	{
		[Outlet]
		UIKit.UIButton collapse { get; set; }

		[Outlet]
		UIKit.UIButton expand { get; set; }

		[Outlet]
		UIKit.UICollectionView items { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (items != null) {
				items.Dispose ();
				items = null;
			}

			if (expand != null) {
				expand.Dispose ();
				expand = null;
			}

			if (collapse != null) {
				collapse.Dispose ();
				collapse = null;
			}
		}
	}
}
